package com.nlmk.directories.impl;

import java.util.function.BiConsumer;

import org.springframework.web.reactive.function.client.WebClient;

import lombok.Getter;

import org.mockito.ArgumentMatchers;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Билд mock<WebClient>.
 * @param <E>
 */
@Getter
public class WebClientBuilder<E>{

    private final WebClient webClient = mock(WebClient.class);

    public void build(
            E e,
            BiConsumer<E, WebClient.ResponseSpec> addToReturn
    ) {
        var uriSpecMock = mock(WebClient.RequestHeadersUriSpec.class);
        var headersSpecMock = mock(WebClient.RequestHeadersSpec.class);
        var responseSpecMock = mock(WebClient.ResponseSpec.class);
        when(webClient.get()).thenReturn(uriSpecMock);
        when(uriSpecMock.uri(ArgumentMatchers.<String>notNull())).thenReturn(headersSpecMock);
        when(headersSpecMock.retrieve()).thenReturn(responseSpecMock);
        addToReturn.accept(e, responseSpecMock);
    }

}
