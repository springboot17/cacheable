package com.nlmk.directories.impl;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.ArgumentMatchers;

import com.nlmk.directories.dto.MaterialResource;
import com.nlmk.directories.service.impl.DirectoriesMaterialResourceServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@DisplayName("Тестирование: DirectoriesMaterialResourceServiceImpl")
class DirectoriesMaterialResourceServiceImplTest {

    private final WebClientBuilder<MaterialResource> webClientBuilder = new WebClientBuilder<>();

    private final DirectoriesMaterialResourceServiceImpl directoriesMaterialResourceService
            = new DirectoriesMaterialResourceServiceImpl(webClientBuilder.getWebClient());


    @Test
    @DisplayName("Тестирование: получение листа материальных ресурсов по коду группы")
    public void testDirectoriesMaterialGetAllByTypeCode() {
        var actual = new MaterialResource();
        webClientBuilder.build(actual, (e, spec) ->
                when(spec.bodyToFlux(ArgumentMatchers.<Class<MaterialResource>>notNull()))
                        .thenReturn(Flux.just(actual, actual)));
        List<MaterialResource> expected = directoriesMaterialResourceService
                .getAllMaterialResourceByTypeCode("code");
        assertNotNull(expected);
        assertEquals(expected.size(), 2);
    }

    @Test
    @DisplayName("Тестирование: получение материального ресурса по коду")
    public void testDirectoriesMaterialTypeByCode() {
        var actual = new MaterialResource();
        webClientBuilder.build(actual, (e, spec) ->
                when(spec.bodyToMono(ArgumentMatchers.<Class<MaterialResource>>notNull()))
                        .thenReturn(Mono.just(actual)));
        var expected = directoriesMaterialResourceService
                .getMaterialResourceByCode("code");
        assertNotNull(expected);
        assertEquals(expected, actual);
    }

}