package com.nlmk.directories.impl;


import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.ArgumentMatchers;

import com.nlmk.directories.dto.MaterialResourceType;
import com.nlmk.directories.service.impl.DirectoriesMaterialResourceTypeServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@DisplayName("Тестирование: DirectoriesMaterialResourceServiceImpl")
class DirectoriesMaterialResourceTypeServiceImplTest {

    private final WebClientBuilder<MaterialResourceType> webClientBuilder = new WebClientBuilder<>();

    private final DirectoriesMaterialResourceTypeServiceImpl directoriesMaterialResourceTypeService
            = new DirectoriesMaterialResourceTypeServiceImpl(webClientBuilder.getWebClient());

    @Test
    @DisplayName("Тестирование: получение листа групп материальных ресурсов")
    public void testDirectoriesMaterialTypeGetAll() {
        var actual = new MaterialResourceType();
        webClientBuilder.build(actual, (e, spec) ->
                when(spec.bodyToFlux(ArgumentMatchers.<Class<MaterialResourceType>>notNull()))
                        .thenReturn(Flux.just(actual, actual)));
        List<MaterialResourceType> expected = directoriesMaterialResourceTypeService
                .getAllMaterialResourceType();
        assertNotNull(expected);
        assertEquals(expected.size(), 2);
    }

    @Test
    @DisplayName("Тестирование: получить группу материальных ресурсов по коду")
    public void testDirectoriesMaterialTypeByCode() {
        var actual = new MaterialResourceType();
        webClientBuilder.build(actual, (e, spec) ->
                when(spec.bodyToMono(ArgumentMatchers.<Class<MaterialResourceType>>notNull()))
                        .thenReturn(Mono.just(actual)));
        var expected = directoriesMaterialResourceTypeService
                .getMaterialResourceTypeByCode("code");
        assertNotNull(expected);
        assertEquals(expected, actual);
    }


}