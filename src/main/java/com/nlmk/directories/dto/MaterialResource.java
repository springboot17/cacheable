package com.nlmk.directories.dto;

import lombok.Data;
/**
 * Ресурсы.
 */
@Data
public class MaterialResource {

    private String name;

    private String code;

}
