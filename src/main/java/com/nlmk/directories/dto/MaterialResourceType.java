package com.nlmk.directories.dto;

import lombok.Data;

/**
 * Группы материалов(ЖРС/ТОПЛИВО/ФЛЮСЫ).
 */
@Data
public class MaterialResourceType {

    private String name;

    private String code;

}
