package com.nlmk.directories.service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import com.nlmk.directories.dto.MaterialResource;
import com.nlmk.directories.dto.MaterialResourceType;

@Log4j2
@Service
@AllArgsConstructor
public class ScheduledBaySend {

    private final DirectoriesMaterialResourceTypeService materialResourceTypeService;
    private final ExecutorService executors = Executors.newFixedThreadPool(100);

    @Scheduled(fixedDelay = 1000L)
    public void send() throws InterruptedException {
//        log.info("запуск");
        for (int i = 0; i < 1000; i++) {
            executors.submit(() -> {
                List<MaterialResourceType> materialResource = materialResourceTypeService
                        .getAllMaterialResourceType();
//                log.info(materialResource);
            });
        }
        log.info("1000 запросов выполнено");
    }

}
