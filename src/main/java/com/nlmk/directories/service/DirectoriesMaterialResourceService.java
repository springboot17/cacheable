package com.nlmk.directories.service;

import java.util.List;

import com.nlmk.directories.dto.MaterialResource;


/**
 * сервис для получения справочников по материалным ресурсам.
 */
public interface DirectoriesMaterialResourceService {

    /**
     * получить конкретный материальные ресурсы по коду.
     *
     * @param code
     *         {@link MaterialResource#getCode()}.
     *
     * @return MaterialResource.
     */
    MaterialResource getMaterialResourceByCode(final String code);

    /**
     * получить материальные ресурсы по коду типа материального ресурса.
     *
     * @param typeCode
     *         {код типа (группы) материального ресурса}.
     *
     * @return MaterialResource
     */
    List<MaterialResource> getAllMaterialResourceByTypeCode(final String typeCode);

}
