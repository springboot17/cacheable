package com.nlmk.directories.service.impl;

import java.time.Duration;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import com.nlmk.directories.config.ConstantCache;
import com.nlmk.directories.dto.MaterialResource;
import com.nlmk.directories.service.DirectoriesMaterialResourceService;

import reactor.util.retry.Retry;

@Service
@AllArgsConstructor
@Log4j2
public class DirectoriesMaterialResourceServiceImpl implements DirectoriesMaterialResourceService {

    private final WebClient webClient;

    @Cacheable(cacheNames = ConstantCache.MATERIAL_RESOURCE, key = "#code")
    @Override public MaterialResource getMaterialResourceByCode(
            String code
    ) {
        return webClient
                .get()
                .uri(String.join("", "/api/material-resource/code/", code))
                .retrieve()
                .bodyToMono(MaterialResource.class)
                .retryWhen(Retry.fixedDelay(3, Duration.ofMillis(100)))
                .doOnError(error -> log.error("An error has occurred {}", error.getMessage())).block();

    }

    @Cacheable(cacheNames = ConstantCache.MATERIAL_RESOURCE_LIST_BY_CODE_GROUP)
    @Override public List<MaterialResource> getAllMaterialResourceByTypeCode(String typeCode) {
        return webClient
                .get()
                .uri(String.join("", "/api/material-resource/list?materialResourceTypeCode.equals=",
                                 typeCode
                ))
                .retrieve()
                .bodyToFlux(MaterialResource.class)
                .retryWhen(Retry.fixedDelay(3, Duration.ofMillis(100)))
                .doOnError(error -> log.error("An error has occurred {}", error.getMessage())).collectList().block();
    }

}
