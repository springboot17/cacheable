package com.nlmk.directories.service.impl;

import java.time.Duration;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import com.nlmk.directories.config.ConstantCache;
import com.nlmk.directories.dto.MaterialResourceType;
import com.nlmk.directories.service.DirectoriesMaterialResourceTypeService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

@Service
@AllArgsConstructor
@Log4j2
public class DirectoriesMaterialResourceTypeServiceImpl implements DirectoriesMaterialResourceTypeService {

    private final WebClient webClient;

    @Cacheable(cacheNames = ConstantCache.MATERIAL_RESOURCE_TYPE, key = "#code")
    @Override public MaterialResourceType getMaterialResourceTypeByCode(String code) {
        return webClient
                .get()
                .uri(String.join("", "/api/material-resource-type/code/", code))
                .retrieve()
                .bodyToMono(MaterialResourceType.class)
                .retryWhen(Retry.fixedDelay(3, Duration.ofMillis(100)))
                .doOnError(error -> log.error("An error has occurred {}", error.getMessage())).block();
    }

    @Cacheable(cacheNames = ConstantCache.MATERIAL_RESOURCE_TYPE_LIST)
    @Override public List<MaterialResourceType> getAllMaterialResourceType() {
        return webClient
                .get()
                .uri("/api/material-resource-type/list")
                .retrieve()
                .bodyToFlux(MaterialResourceType.class)
                .retryWhen(Retry.fixedDelay(3, Duration.ofMillis(100)))
                .doOnError(error -> log.error("An error has occurred {}", error.getMessage())).collectList().block();
    }

}
