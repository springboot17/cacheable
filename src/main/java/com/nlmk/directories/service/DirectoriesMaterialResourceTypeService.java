package com.nlmk.directories.service;

import java.util.List;

import com.nlmk.directories.dto.MaterialResourceType;

/**
 * сервис для получения справочников по материальным ресурсам.
 */
public interface DirectoriesMaterialResourceTypeService {


    /**
     * получить Тип (группу) материального ресурса по коду.
     *
     * @param code
     *         {код типа (группы) материального ресурса}.
     *
     * @return  {@link MaterialResourceType}
     */
    MaterialResourceType getMaterialResourceTypeByCode(final String code);

    /**
     * получить Типы(группы) материальных ресурсов.
     *
     * @return {@link List<MaterialResourceType>}
     */
    List<MaterialResourceType> getAllMaterialResourceType();

}
