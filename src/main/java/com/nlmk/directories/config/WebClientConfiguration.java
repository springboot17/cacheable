package com.nlmk.directories.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.log4j.Log4j2;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;

@Configuration
@Log4j2
public class WebClientConfiguration {


    @Value("${integration.dictionary-nsi.timeout}")
    public int TIMEOUT;
    @Value("${integration.dictionary-nsi.url}")
    private String BASE_URL;

    @Bean
    public WebClient webClientWithTimeout() {
        log.info("BASE_URL = {}", BASE_URL);
        return WebClient.builder()
                .baseUrl(BASE_URL)
                .defaultHeader(MediaType.TEXT_EVENT_STREAM_VALUE)
                .clientConnector(new ReactorClientHttpConnector(
                        HttpClient.create()
                                .option(
                                        ChannelOption.CONNECT_TIMEOUT_MILLIS,
                                        TIMEOUT * 1000
                                )
                                .doOnConnected(c -> c.addHandlerLast(new ReadTimeoutHandler(TIMEOUT))
                                        .addHandlerLast(new WriteTimeoutHandler(TIMEOUT))))).build();
    }

}
