package com.nlmk.directories.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.DiskStoreConfiguration;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

@Configuration
@EnableCaching
public class EhCacheConfig {

    @Bean
    public EhCacheCacheManager ehCacheManager() {
        EhCacheCacheManager ehCacheCacheManager = new EhCacheCacheManager();
        ehCacheCacheManager.setCacheManager(getCustomCacheManager());
        return ehCacheCacheManager;
    }

    private CacheManager getCustomCacheManager() {
        CacheManager cacheManager = CacheManager.create(getEhCacheConfiguration());
        cacheManager.addCache(createCache(ConstantCache.MATERIAL_RESOURCE));
        cacheManager.addCache(createCache(ConstantCache.MATERIAL_RESOURCE_LIST_BY_CODE_GROUP));
        cacheManager.addCache(createCache(ConstantCache.MATERIAL_RESOURCE_TYPE));
        cacheManager.addCache(createCache(ConstantCache.MATERIAL_RESOURCE_TYPE_LIST));
        return cacheManager;
    }

    private Cache createCache(String cacheName) {
        CacheConfiguration cacheConfig = new CacheConfiguration(cacheName, 100)
                .maxEntriesLocalHeap(100)
                .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LRU)
                .eternal(false)
                .timeToLiveSeconds(1800)
                .timeToIdleSeconds(1800);
        return new Cache(cacheConfig);
    }

    private net.sf.ehcache.config.Configuration getEhCacheConfiguration() {
        net.sf.ehcache.config.Configuration configuration = new net.sf.ehcache.config.Configuration();
        DiskStoreConfiguration diskStoreConfiguration = new DiskStoreConfiguration();
        diskStoreConfiguration.setPath("java.io.tmpdir");
        configuration.addDiskStore(diskStoreConfiguration);
        return configuration;
    }

}

