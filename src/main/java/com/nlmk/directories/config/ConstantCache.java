package com.nlmk.directories.config;

/**
 * константы кэширования.
 */
public class ConstantCache {

    public final static String MATERIAL_RESOURCE = "materialResource";
    public final static String MATERIAL_RESOURCE_LIST_BY_CODE_GROUP = "materialResourceListByCodeGroup";
    public final static String MATERIAL_RESOURCE_TYPE = "materialResourceType";
    public final static String MATERIAL_RESOURCE_TYPE_LIST = "materialResourceTypeList";

}
