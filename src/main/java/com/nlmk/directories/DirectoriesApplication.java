package com.nlmk.directories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.nlmk.directories.service.ScheduledBaySend;

@SpringBootApplication
@EnableScheduling
public class DirectoriesApplication {


    public static void main(String[] args) {
        SpringApplication.run(DirectoriesApplication.class, args);
    }



}
